package com.richgold.richgold_wifi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.richgold.richgold_wifi.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_NORMAL = 10000;
    private View layout;
    private final static String TAG = MainActivity.class.getSimpleName();
    /**
     * define variable
     */
    boolean wasAPEnabled = false;
    static WifiAp wifiAp;
    private WifiManager wifi;
    static Button btnWifiToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set full screen, no title and status bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Log.d(TAG, "#onCreate() - ");
        setContentView(R.layout.activity_main);

        SurfaceView surfaceView = (SurfaceView) this.findViewById(R.id.surfaceView);
        layout = this.findViewById(R.id.buttonlayout);
        layout.setVisibility(ViewGroup.VISIBLE);


        // check permission
        if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "#onCreate() - request permission");
            requestPermissions(new String[]{Manifest.permission.ACCESS_WIFI_STATE}, REQUEST_NORMAL);
            requestPermissions(new String[]{Manifest.permission.CHANGE_WIFI_STATE}, REQUEST_NORMAL);
        } else {
            Log.d(TAG, "#onCreate() - granted");
            surfaceView.setVisibility(View.VISIBLE);
        }


        btnWifiToggle = (Button) findViewById(R.id.btnWifiToggle);
        wifiAp = new WifiAp();
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        btnWifiToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                wifiAp.toogleWiFiAP(wifi, MainActivity.this);
            }
        });

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DIM_BEHIND);


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public static void updateStatusDisplay() {
        if (wifiAp.getWifiAPState() == wifiAp.WIFI_AP_STATE_ENABLED ||
            wifiAp.getWifiAPState() == wifiAp.WIFI_AP_STATE_ENABLING ) {
            btnWifiToggle.setText("Turn off");
        }
        else
        {
            btnWifiToggle.setText("Turn on");
        }
    }

    public void doSomthing(View v) {
        switch (v.getId())  {
            case R.id.btnWifiToggle:
                Log.d(TAG, "#doSomething - btnWifiToggle");
                break;
        }
    }
}